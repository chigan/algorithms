// Чиганов А.В., АПО-13
// 2_1. Даны два массива целых чисел одинаковой длины 
// A[0..n­1] и B[0..n­1]. Необходимо найти первую пару 
// индексов i0 и j0, i0 <= j0, такую что A[i0] + B[j0] = max {A[i] + B[j],
// где 0 <= i < n, 0 <= j < n, i <= j}. Время работы ­ O(n). 
// in 
// 4 
// 4 ­-8 6 0 
// ­-10 3 1 1
// out
// 0 1   

#include <iostream>
using namespace std;
int main(int argc, char *argv[])
{    
	int n;
	cin >> n;
	int *a = new int[n];
	for(int i=0;i<n;++i)
		cin >> a[i];
    int *b = new int[n];
    for(int i=0;i<n;++i)
        cin >> b[i];

    int i_0 = 0, j_0 = 0;
    int i_max_a = 0;
    int max_s=a[0] + b[0];
	for(int i=0; i<n; ++i)
    {
        if (a[i] > a[i_max_a])
            {
                i_max_a = i;
            }
        int sum = a[i_max_a]+b[i];
        if (sum > max_s) 
        {
            max_s = sum;
            j_0 = i;
            i_0 = i_max_a;
        }
    }
    cout << i_0 << " " << j_0 << "\n";

    delete [] a;
    delete [] b;

    return 0;
}