// Чиганов А.В., АПО-13
// 6_3. Тупики.
// На вокзале есть некоторое количество тупиков, куда прибывают электрички.
// Этот вокзал является их конечной станцией. Дано расписание движения 
// электричек, в котором для каждой электрички указано время ее прибытия, 
// а также время отправления в следующий рейс. Электрички в расписании
// упорядочены по времени прибытия. Когда электричка прибывает, ее ставят 
// в свободный тупик с минимальным номером. При этом если электричка из 
// какого-то тупика отправилась в момент времени X, то электричку, которая 
// прибывает в момент времени X, в этот тупик ставить нельзя, а электричку,
// прибывающую в момент X+1 — можно.
// В данный момент на вокзале достаточное количество тупиков для работы по 
// расписанию. Напишите программу, которая по данному расписанию определяет, 
// какое минимальное количество тупиков требуется для работы вокзала.
// Формат входных данных. Вначале вводится n - количество электричек в 
// расписании. Затем вводится n строк для каждой электрички, в строке - 
// время прибытия и время отправления. Время - натуральное число от 0 до 10^9. 
// Строки в расписании упорядочены по времени прибытия. Формат выходных данных. 
// Натуральное число - минимальное количеством тупиков.
// Максимальное время: 50мс, память: 5Мб.

#include <iostream>
#include <assert.h>

using namespace std;

class Heap 
{
private:
	int* Buffer;
	int BufferSize;
	int RealSize;

public:
	Heap() { Buffer = NULL; BufferSize = 0; RealSize = 0; }
	Heap(int size);
	~Heap();
	int size() { return RealSize; }
	int show_min();
	void add(int element);
	void extract_min();

private:
	void SiftUp(int index);
	void SiftDown(int index);
};

int main() 
{
	int size = 0;
	cin >> size;
	int dep_t = 0;	
	int arr_t = 0;
	int deadends = 0;
	Heap dep_time(size);
	for (int i = 0; i < size; i++) 
	{
		cin >> arr_t;
		cin >> dep_t;
		while (dep_time.size() > 0 && arr_t > dep_time.show_min()) 
		{
			dep_time.extract_min();
		}
		dep_time.add(dep_t);
		deadends = (dep_time.size() > deadends)?dep_time.size():deadends;
	}
	cout << deadends << endl;
	return 0;
}

Heap::Heap(int size) 
{
	RealSize = 0;
	BufferSize = 0;
	Buffer = new int[size];
}

Heap::~Heap()
{
	delete[] Buffer;
	Buffer = NULL;
}



void Heap::add(int element) 
{
	assert(Buffer!=NULL);
	Buffer[RealSize] = element;
	SiftUp(RealSize);
	RealSize++;	
}

void Heap::extract_min() 
{
	assert(Buffer != NULL && RealSize > 0);
	--RealSize;
	Buffer[0] = Buffer[RealSize];
	SiftDown(0);
}

void Heap::SiftUp(int index) 
{
	while (index > 0) 
	{
		int upper = (index - 1)/2;
		if (Buffer[upper] <= Buffer[index])
			return;
		swap(Buffer[upper], Buffer[index]);
		index=upper;
	}
}

void Heap::SiftDown(int index) 
{
	int left = index*2 + 1;
	int right = left +1;
	int min = index;
	if (left < RealSize && Buffer[index] > Buffer[left])
		min = left;
	if (right < RealSize && Buffer[min] > Buffer[right])
		min = right;
	if (min != index) 
	{
		swap(Buffer[min], Buffer[index]);
		SiftDown(min);
	}
}

int Heap::show_min() 
{
	assert(Buffer != NULL && RealSize > 0);
	return Buffer[0];
}