// Чиганов А.В., АПО-13
// 4_3. Реализовать очередь с помощью стека. Использовать стек, 
// реализованный с помощью динамического буфера. Во всех задачах 
// из следующего списка следует написать структуру данных, обрабатывающую 
// команды push* и pop*. Формат входных данных. В первой строке количество 
// команд. Каждая команда задаётся как 2 целых числа: a b.
// a=1-push front
// a=2-pop front
// a=3-push back
// a=4-pop back
// Для очереди используются команды 2 и 3. Для дека используются 
// все четыре команды. Если дана команда pop*, то число b - ожидаемое 
// значение. Если команда pop вызвана для пустой структуры данных, то 
// ожидается “-1”. Формат выходных данных. Требуется напечатать YES - 
// если все ожидаемые значения совпали. Иначе, если хотя бы одно ожидание 
// не оправдалось, то напечатать NO.

#include <iostream>
#include <assert.h>

using namespace std;

class Stack 
{
private: 
    int* Buffer;
    int RealSize;
    int BufferSize;

public: 
    Stack() {BufferSize = 0; RealSize = 0; Buffer = NULL;}
    Stack(int new_size);
    ~Stack();
    int Is_Empty();
    int Get(int index);
    int Size() {return RealSize;}
    int operator[](int index) {return Get(index);}
    void push(int element);
    int pop();

private: 
    void grow();
};

class Queue 
{
private: 
    Stack front, back;

public: 
    int Get(int index);
    int operator[](int index) {return Get(index);}
    void push_back(int element);
    int pop_front();
};

int main(int argc, char *argv[]) 
{
    int size = 0;
    cin >> size;
    Queue queue;
    int command = 0;
    int value = 0;
    for (int i = 0; i < size; ++i) 
    {
        cin >> command;
        cin >> value;
        switch (command) 
        {
        case 2:
            if (value != queue.pop_front()) 
            {
                cout << "NO" << endl;
                return 0;
            }
            break;
        case 3:
            queue.push_back(value);
            break;
        }
    }
    cout << "YES" << endl;
    return 0;
}

Stack::Stack(int new_size) 
{
    BufferSize = new_size;
    RealSize = 0;
    Buffer = new int[BufferSize];
}

Stack::~Stack() 
{
    RealSize = 0;
    BufferSize = 0;
    delete[] Buffer;
    Buffer = NULL;
}

int Stack::Get(int index) 
{
    assert(index >= 0 && index <= RealSize && Buffer != NULL);
    return Buffer[index];
}

void Stack::grow() 
{
    if (BufferSize == 0) 
        BufferSize = 1;
    else 
        BufferSize*= 2;
    int* NewBuffer = new int[BufferSize];
    for (int i = 0; i < RealSize; ++i) 
    {
        NewBuffer[i] = Buffer[i];
    }
    delete[] Buffer;
    Buffer = NewBuffer;
}

int Stack::pop() 
{
    assert(RealSize > 0 && Buffer != NULL);
    RealSize--;
    return Get(RealSize);
}

void Stack::push(int element) 
{
    if (RealSize == BufferSize) 
        grow();
    assert(RealSize < BufferSize && Buffer != NULL);
    Buffer[RealSize] = element;
    RealSize++;
}

int Stack::Is_Empty() 
{
    if (RealSize == 0) 
        return 1;
    else
        return 0;
}

void Queue::push_back(int element) 
{
    front.push(element);
}

int Queue::pop_front() 
{
    if (back.Is_Empty() && front.Is_Empty()) 
        return -1;
    if (back.Is_Empty()) 
    {
        while (!front.Is_Empty()) 
        {
            back.push(front.pop());
        }
    }
	return back.pop();    
}

int Queue::Get(int index) 
{
    assert(index < back.Size() + front.Size());
    assert(!front.Is_Empty() || !back.Is_Empty());

    if (!front.Is_Empty() && back.Is_Empty()) 
    {
        return front.Get(index);
    }
    if (!front.Is_Empty() && !back.Is_Empty()) 
    {
        if (index < back.Size()) 
            return back.Get(back.Size() - 1 - index);
        if (index >= back.Size()) 
            return front.Get(index - back.Size());
    }
    if (front.Is_Empty() && !back.Is_Empty()) 
    {
        return back.Get(back.Size() - 1 - index);
    }
}
