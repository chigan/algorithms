// Чиганов А.В., АПО-13
// 5_1. Скобочная последовательность. 
// Дан фрагмент последовательности скобок, состоящей из символов (){}[]. 
// Требуется определить, возможно ли продолжить фрагмент в обе стороны, 
// получив корректную последовательность. Формат входных данных. Строка, 
// содержащая символы (){}[] и, возможно, перевод строки. Максимальная длина
// строки 10^6 символов. Формат выходных данных. Если возможно ­ вывести
// минимальную корректную последовательность, иначе ­напечатать "IMPOSSIBLE". 
// in  			out 
// {}[[[[{}[]  {}[[[[{}[]]]]] 
// {][[[[{}[]  IMPOSSIBLE 
// ]()}[](({}  {[]()}[](({})) 


#include <iostream>
#include <assert.h>

using namespace std;

class Stack 
{
private: 
    char* Buffer;
    int RealSize;
    int BufferSize;

public: 
    Stack() {BufferSize = 0; RealSize = 0; Buffer = NULL;}
    Stack(int new_size);
    ~Stack();
    int Is_Empty();
    char Get(int index);
    int Size() {return RealSize;}
    char operator[](int index) {return Get(index);}
    void push(char element);
    char pop();

private: 
    void grow();
};

char reflection(char input)
{
	switch (input) 
    {
    case '}' : return '{';
    case ')' : return '(';
    case ']' : return '[';
    case '{' : return '}';
    case '(' : return ')';
    case '[' : return ']';
    }
}

bool is_opening_bracket(char input)
{
	if ((input=='{')||(input=='[')||(input=='('))
		return 1;
	else
		return 0;
}

int main(int argc, char *argv[]) 
{
	const int max_length = 1000000;
	char* in_str = new char [max_length];
	cin >> in_str;
	int length = 0; 
	while ((in_str[length]!='\0')||(length==max_length-1))
	{
		++length;
	}

	Stack in_stack(length);
	Stack to_left;
	Stack to_right;

	for (int i = 0; i < length; ++i)
	{
		char buf_char=in_str[i];
		if (is_opening_bracket(buf_char))
		{
			in_stack.push(buf_char);
		}
		else
		{
			if(in_stack.Is_Empty())
			{
				to_left.push(reflection(buf_char));
			}
			else
			{
				char buf_pop = in_stack.pop();
					if (buf_char != reflection(buf_pop))
					{
						cout << "IMPOSSIBLE";
						delete [] in_str;
						return 0;
					}
			}
		}
	}

	while (!to_left.Is_Empty())
		cout << to_left.pop();
	cout << in_str;
	while (!in_stack.Is_Empty())
		cout << reflection(in_stack.pop());

	cout << endl;
	delete [] in_str;
	return 0;
}

Stack::Stack(int new_size) 
{
    BufferSize = new_size;
    RealSize = 0;
    Buffer = new char[BufferSize];
}

Stack::~Stack() 
{
    RealSize = 0;
    BufferSize = 0;
    delete[] Buffer;
    Buffer = NULL;
}

char Stack::Get(int index) 
{
    assert(index >= 0 && index <= RealSize && Buffer != NULL);
    return Buffer[index];
}

void Stack::grow() 
{
    if (BufferSize == 0) 
    	BufferSize = 1;
    else 
        BufferSize*= 2;
    char* NewBuffer = new char[BufferSize];
    for (int i = 0; i < RealSize; i++) 
    {
        NewBuffer[i] = Buffer[i];
    }
    delete[] Buffer;
    Buffer = NewBuffer;
}

char Stack::pop() 
{
    assert(RealSize > 0 && Buffer != NULL);
    RealSize--;
    return Get(RealSize);
}

void Stack::push(char element) 
{
    if (RealSize == BufferSize) 
    	grow();
    assert(RealSize < BufferSize && Buffer != NULL);
    Buffer[RealSize] = element;
    RealSize++;
}

int Stack::Is_Empty() 
{
    if (RealSize == 0) 
    	return 1;
    else
        return 0;
}

