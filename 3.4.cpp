// Чиганов А.В., АПО-13
// 3_4. Дан отсортированный массив целых чисел A[0..n­1] 
// и массив целых чисел B[0..m­1]. Для каждого 
// элемента массива B[i] найдите минимальный индекс 
// элемента массива A[k], ближайшего по значению к B[i]. 
// Время работы поиска для каждого элемента B[i]: O(log(k)). 
// n  
// 3 
// 10 20 30 
// 3 
// 9 18 35  
// out 
// 0 1 2

#include <iostream>
#include <limits.h>
#include <stdlib.h>

using namespace std;

int binary_search(int *a, int n, int x)
{
	int diff = INT_MAX;
	int first = 0;
	int last = n;
		
	while (first < last)
	{
        int mid = (first + last) / 2;
       	if (abs(a[mid+1] - x) > abs(a[mid] - x))
			last = mid;
       	else
       	    first = mid + 1;
    }
    return last;
}

int main(int argc, char *argv[])
{    
	int n;
	cin >> n;
	int *a = new int[n];
	for(int i=0;i<n;++i)
		cin >> a[i];

    int m;
    cin >> m;
    int *b = new int[m];
    for(int i=0;i<m;++i)
        cin >> b[i];
	for(int i=0; i<m; ++i)
    {
    	cout << binary_search(a, n, b[i]) << " ";
    }

    delete [] a;
    delete [] b;
    return 0;
}