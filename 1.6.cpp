// Чиганов А.В., АПО-13
// 1_6. Дан массив целых чисел A[0..n). 
// Не используя других массивов переставить элементы массива A  в 
// обратном порядке за O(n). Пример: 
// in 
// 4 
// 2 ­5 9 3 
// out 
// 3 9 ­5 2 

#include <iostream>
using namespace std;
int main(int argc, char *argv[])
{    
	int n;
	cin >> n;
	int *a = new int[n];
	for(int i=0;i<n;++i)
 		cin >> a[i];

 	--n;
	for(int i=0; i<=n/2; ++i)
    {
    	int buf = a[i];
    	a[i] = a[n-i];
    	a[n-i] = buf;
    }

    for(int i=0;i<n+1;i++)
 		cout << a[i] << " ";
 	cout << "\n";

    delete [] a;

    return 0;
}